﻿using ConnectFive.ServerLogic;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ConnectFive.TestClient
{
    public partial class MainForm : Form
    {
        private const string player1Name = "Player 1";
        private const string player2Name = "Player 2";

        private readonly Server server = new Server();

        private readonly Label[][] cellLabels;
        private readonly int rowCount;
        private readonly int columnCount;

        private string PlayerOnTurn => Player1.State == PlayerState.PlayerTurn ? player1Name : player2Name;
        private Cell[][] Cells => Player1.Game.Cells;
        private Player Player1 => server.GetPlayer(player1Name);

        public MainForm()
        {
            InitializeComponent();

            server.StartGame(player1Name);
            server.StartGame(player2Name);

            Cell[][] cells = Cells;
            rowCount = cells.Length;
            columnCount = cells.First().Length;

            cellLabels = new Label[rowCount][];
            for (int row = 0; row < rowCount; row++)
            {
                Label[] cellLabelsRow = new Label[columnCount];

                for (int column = 0; column < columnCount; column++)
                {
                    var cellLabel = new Label
                    {
                        AutoSize = cellTemplateLabel.AutoSize,
                        BackColor = cellTemplateLabel.BackColor,
                        BorderStyle = cellTemplateLabel.BorderStyle,
                        Font = cellTemplateLabel.Font,
                        Location = new Point(column * cellTemplateLabel.Size.Width, row * cellTemplateLabel.Size.Height),
                        Size = cellTemplateLabel.Size,
                        TextAlign = cellTemplateLabel.TextAlign,
                        Tag = new Point(row, column)
                    };

                    cellLabel.Click += OnCellLabelClick;

                    cellLabelsRow[column] = cellLabel;
                    cellsPanel.Controls.Add(cellLabel);
                }

                cellLabels[row] = cellLabelsRow;
            }

            UpdatePlayerOnTurnLabel();
        }

        private void UpdatePlayerOnTurnLabel()
        {
            playerLabel.Text = $"{PlayerOnTurn} is on turn.";
        }

        private void OnCellLabelClick(object sender, EventArgs e)
        {
            var coordinates = (Point)(sender as Control).Tag;

            try
            {
                server.Play(PlayerOnTurn, coordinates.X, coordinates.Y);
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Cannot play on this cell.");
                return;
            }

            UpdateCellLabels();
            UpdatePlayerOnTurnLabel();

            Player winner = Player1.Game.Winner;
            if (winner != null)
            {
                MessageBox.Show($"{winner.Name} has won!");
            }
        }

        private void UpdateCellLabels()
        {
            Cell[][] cells = Cells;
            for (int row = 0; row < rowCount; row++)
            {
                for (int column = 0; column < columnCount; column++)
                {
                    cellLabels[row][column].Text = GetCellLabel(cells[row][column]);
                }
            }
        }

        private static string GetCellLabel(Cell cell)
        {
            switch (cell)
            {
                case Cell.Empty:
                    return "";

                case Cell.Player1:
                    return "O";

                default:
                    Debug.Assert(cell == Cell.Player2);
                    return "X";
            }
        }

        private void OnRestartButtonClick(object sender, EventArgs e)
        {
            server.LeaveGame(player1Name);
            server.LeaveGame(player2Name);

            server.StartGame(player1Name);
            server.StartGame(player2Name);

            UpdateCellLabels();
            UpdatePlayerOnTurnLabel();
        }
    }
}
