﻿namespace ConnectFive.TestClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.playerLabel = new System.Windows.Forms.Label();
            this.cellsPanel = new System.Windows.Forms.Panel();
            this.cellTemplateLabel = new System.Windows.Forms.Label();
            this.restartButton = new System.Windows.Forms.Button();
            this.cellsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // playerLabel
            // 
            this.playerLabel.AutoSize = true;
            this.playerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.playerLabel.Location = new System.Drawing.Point(12, 9);
            this.playerLabel.Name = "playerLabel";
            this.playerLabel.Size = new System.Drawing.Size(90, 20);
            this.playerLabel.TabIndex = 0;
            this.playerLabel.Text = "playerLabel";
            // 
            // cellsPanel
            // 
            this.cellsPanel.Controls.Add(this.cellTemplateLabel);
            this.cellsPanel.Location = new System.Drawing.Point(12, 42);
            this.cellsPanel.Name = "cellsPanel";
            this.cellsPanel.Size = new System.Drawing.Size(800, 800);
            this.cellsPanel.TabIndex = 1;
            // 
            // cellTemplateLabel
            // 
            this.cellTemplateLabel.BackColor = System.Drawing.Color.White;
            this.cellTemplateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cellTemplateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cellTemplateLabel.Location = new System.Drawing.Point(0, 0);
            this.cellTemplateLabel.Name = "cellTemplateLabel";
            this.cellTemplateLabel.Size = new System.Drawing.Size(20, 20);
            this.cellTemplateLabel.TabIndex = 0;
            this.cellTemplateLabel.Text = "X";
            this.cellTemplateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cellTemplateLabel.Visible = false;
            // 
            // restartButton
            // 
            this.restartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.restartButton.Location = new System.Drawing.Point(160, 6);
            this.restartButton.Name = "restartButton";
            this.restartButton.Size = new System.Drawing.Size(80, 30);
            this.restartButton.TabIndex = 2;
            this.restartButton.Text = "Restart";
            this.restartButton.UseVisualStyleBackColor = true;
            this.restartButton.Click += new System.EventHandler(this.OnRestartButtonClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 854);
            this.Controls.Add(this.restartButton);
            this.Controls.Add(this.cellsPanel);
            this.Controls.Add(this.playerLabel);
            this.Name = "MainForm";
            this.Text = "Connect Five";
            this.cellsPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label playerLabel;
        private System.Windows.Forms.Panel cellsPanel;
        private System.Windows.Forms.Label cellTemplateLabel;
        private System.Windows.Forms.Button restartButton;
    }
}

