﻿using ConnectFive.ServerLogic;
using Microsoft.Extensions.DependencyInjection;

namespace ConnectFive.Api.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterDependencies(this IServiceCollection services)
        {
            services.AddSingleton<IServer, Server>();

            return services;
        }
    }
}