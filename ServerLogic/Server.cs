﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConnectFive.ServerLogic
{
    public class Server : IServer
    {
        private readonly Dictionary<string, Player> players = new Dictionary<string, Player>();
        private readonly List<Game> games = new List<Game>();

        public Player GetPlayer(string playerName)
        {
            if (!players.ContainsKey(playerName))
            {
                players.Add(playerName, new Player(playerName));
            }

            return players[playerName];
        }

        public void StartGame(string playerName)
        {
            Player player = GetPlayer(playerName);
            if (player.State != PlayerState.OutOfGame)
            {
                throw new InvalidOperationException();
            }

            Player waitingOpponent = players.Values.SingleOrDefault(p => 
                p.Name != playerName && 
                p.State == PlayerState.WaitingForOpponent);

            StartGame(player, waitingOpponent);
        }

        private void StartGame(Player player, Player waitingOpponent)
        {
            if (waitingOpponent == null)
            {
                player.State = PlayerState.WaitingForOpponent;
            }
            else
            {
                var game = new Game(player, waitingOpponent);

                player.Game = game;
                waitingOpponent.Game = game;

                games.Add(game);
            }
        }

        public void Play(string playerName, int row, int column)
        {
            Player player = GetPlayer(playerName);
            if (player.State != PlayerState.PlayerTurn)
            {
                throw new InvalidOperationException();
            }

            Play(player, player.Game, row, column);
        }

        private void Play(Player player, Game game, int row, int column)
        {
            game.Play(row, column);

            if (game.Winner != null)
            {
                games.Remove(game);
            }
        }

        public void LeaveGame(string playerName)
        {
            Player player = GetPlayer(playerName);
            LeaveGame(player, player.Game);
        }

        private void LeaveGame(Player player, Game game)
        {
            if (game != null)
            {
                game.Leave(player);
                games.Remove(game);
            }

            player.Game = null;
            player.State = PlayerState.OutOfGame;
        }
    }
}
