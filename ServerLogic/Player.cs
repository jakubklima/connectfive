﻿namespace ConnectFive.ServerLogic
{
    public class Player
    {
        public string Name { get; }
        public PlayerState State { get; internal set; } = PlayerState.OutOfGame;
        public Game Game { get; internal set; }

        internal Player(string name)
        {
            Name = name;
        }
    }
}
