﻿namespace ConnectFive.ServerLogic
{
    public enum PlayerState
    {
        OutOfGame,
        WaitingForOpponent,
        PlayerTurn,
        OpponentTurn,
        GameEnded
    }
}
