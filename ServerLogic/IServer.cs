﻿namespace ConnectFive.ServerLogic
{
    public interface IServer
    {
        Player GetPlayer(string playerName);
        void LeaveGame(string playerName);
        void Play(string playerName, int row, int column);
        void StartGame(string playerName);
    }
}