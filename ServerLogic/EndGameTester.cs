﻿using System.Collections.Generic;
using System.Linq;

namespace ConnectFive.ServerLogic
{
    internal class EndGameTester
    {
        private readonly Cell[][] cells;
        private readonly int rowCount;
        private readonly int columnCount;
        private readonly Cell targetCellType;

        internal EndGameTester(Cell[][] cells, Cell targetCellType)
        {
            this.cells = cells;
            this.targetCellType = targetCellType;

            rowCount = this.cells.Length;
            columnCount = this.cells.First().Length;
        }

        internal bool GameEnded()
        {
            return GetSeriesToCheck().Any(IsWinning);
        }

        private bool IsWinning(IEnumerable<Cell> cellSeries)
        {
            const int cellsToWin = 5;
            int touchingCells = 0;

            foreach (Cell cell in cellSeries)
            {
                if (cell == targetCellType)
                {
                    touchingCells++;
                    if (touchingCells == cellsToWin)
                    {
                        return true;
                    }
                }
                else
                {
                    touchingCells = 0;
                }
            }

            return false;
        }

        private IEnumerable<IEnumerable<Cell>> GetSeriesToCheck()
        {
            return GetRowSeriesToCheck()
                .Concat(GetColumnSeriesToCheck())
                .Concat(GetDiagonalSeriesFromTopLeftCorner())
                .Concat(GetDiagonalSeriesFromTopRightCorner());
        }

        private IEnumerable<IEnumerable<Cell>> GetRowSeriesToCheck()
        {
            for (int row = 0; row < rowCount; row++)
            {
                var series = new List<Cell>();
                for (int column = 0; column < columnCount; column++)
                {
                    series.Add(cells[row][column]);
                }
                yield return series;
            }
        }

        private IEnumerable<IEnumerable<Cell>> GetColumnSeriesToCheck()
        {
            for (int column = 0; column < columnCount; column++)
            {
                var series = new List<Cell>();
                for (int row = 0; row < rowCount; row++)
                {
                    series.Add(cells[row][column]);
                }
                yield return series;
            }
        }

        private IEnumerable<IEnumerable<Cell>> GetDiagonalSeriesFromTopLeftCorner()
        {
            return GetDiagonalSeriesFromTopLeftCornerFromLeft()
                .Concat(GetDiagonalSeriesFromTopLeftCornerFromTop());
        }

        private IEnumerable<IEnumerable<Cell>> GetDiagonalSeriesFromTopLeftCornerFromLeft()
        {
            for (int row = 0; row < rowCount; row++)
            {
                var series = new List<Cell>();
                for (int column = 0; column < columnCount; column++)
                {
                    if (row + column >= rowCount)
                    {
                        break;
                    }

                    series.Add(cells[row + column][column]);
                }
                yield return series;
            }
        }

        private IEnumerable<IEnumerable<Cell>> GetDiagonalSeriesFromTopLeftCornerFromTop()
        {
            for (int column = 0; column < columnCount; column++)
            {
                var series = new List<Cell>();
                for (int row = 0; row < rowCount; row++)
                {
                    if (column + row >= columnCount)
                    {
                        break;
                    }

                    series.Add(cells[row][column + row]);
                }
                yield return series;
            }
        }

        private IEnumerable<IEnumerable<Cell>> GetDiagonalSeriesFromTopRightCorner()
        {
            return GetDiagonalSeriesFromTopRightCornerFromLeft()
                .Concat(GetDiagonalSeriesFromTopRightCornerFromBottom());
        }

        private IEnumerable<IEnumerable<Cell>> GetDiagonalSeriesFromTopRightCornerFromLeft()
        {
            for (int row = 0; row < rowCount; row++)
            {
                var series = new List<Cell>();
                for (int column = 0; column < columnCount; column++)
                {
                    if (row - column < 0)
                    {
                        break;
                    }

                    series.Add(cells[row - column][column]);
                }
                yield return series;
            }
        }

        private IEnumerable<IEnumerable<Cell>> GetDiagonalSeriesFromTopRightCornerFromBottom()
        {
            for (int column = 0; column < columnCount; column++)
            {
                var series = new List<Cell>();
                for (int row = 0; row < rowCount; row++)
                {
                    if (column + row >= columnCount)
                    {
                        break;
                    }

                    series.Add(cells[rowCount - 1 - row][column + row]);
                }
                yield return series;
            }
        }
    }
}
