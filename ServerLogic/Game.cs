﻿using System;
using System.Diagnostics;

namespace ConnectFive.ServerLogic
{
    public class Game
    {
        private static readonly Random random = new Random();

        public Player Player1 { get; }
        public Player Player2 { get; }
        public Player Winner { get; private set; }
        public bool OponentLeft { get; private set; }

        public Cell[][] Cells { get; } = InitializeCells();

        internal Game(Player player1, Player player2)
        {
            if (random.Next(2) == 0)
            {
                Swap(ref player1, ref player2);
            }

            Player1 = player1;
            Player1.State = PlayerState.PlayerTurn;

            Player2 = player2;
            Player2.State = PlayerState.OpponentTurn;
        }

        private static Cell[][] InitializeCells()
        {
            const int sideLength = 40; // Fits to the desktop screen.
            var result = new Cell[sideLength][];
            for (int row = 0; row < result.Length; row++)
            {
                result[row] = new Cell[sideLength];
            }
            return result;
        }

        private static void Swap<T>(ref T a, ref T b)
        {
            T temp = a;
            a = b;
            b = temp;
        }

        internal void Play(int row, int column)
        {
            Debug.Assert(
                (Player1.State == PlayerState.PlayerTurn && Player2.State == PlayerState.OpponentTurn) ||
                (Player2.State == PlayerState.PlayerTurn && Player1.State == PlayerState.OpponentTurn));

            Cell targetCell = Player1.State == PlayerState.PlayerTurn ? Cell.Player1 : Cell.Player2;
            MarkCell(row, column, targetCell);

            if (new EndGameTester(Cells, targetCell).GameEnded())
            {
                Winner = targetCell == Cell.Player1 ? Player1 : Player2;
                Player1.State = PlayerState.GameEnded;
                Player2.State = PlayerState.GameEnded;
            }
            else
            {
                SwapStates(Player1, Player2);
            }
        }

        private void MarkCell(int row, int column, Cell targetCell)
        {
            if (row < 0 || Cells.Length <= row)
            {
                throw new InvalidOperationException();
            }

            Cell[] rowCells = Cells[row];
            if (column < 0 || rowCells.Length <= column || rowCells[column] != Cell.Empty)
            {
                throw new InvalidOperationException();
            }

            rowCells[column] = targetCell;
        }

        private static void SwapStates(Player a, Player b)
        {
            PlayerState tempState = a.State;
            a.State = b.State;
            b.State = tempState;
        }

        internal void Leave(Player player)
        {
            if (Winner == null)
            {
                Winner = Player1 == player ? Player2 : Player1;
                Winner.State = PlayerState.GameEnded;
                OponentLeft = true;
            }
        }
    }
}
