﻿using System;
using ConnectFive.ServerLogic;
using Microsoft.AspNetCore.Mvc;

namespace ConnectFive.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class GamesController : Controller
    {
        private readonly IServer _server;

        public GamesController(IServer server)
        {
            _server = server ?? throw new ArgumentNullException(nameof(server));
        }

        //api/games/start/trololord
        [HttpGet("{playerName}")]
        public void Start(string playerName)
        {
            _server.StartGame(playerName);
        }

        //api/games/play/trololord/1/1
        [HttpGet("{playerName}/{row}/{column}")]
        public void Play(string playerName, int row, int column)
        {
            _server.Play(playerName, row, column);
        }

        //api/games/leave/trololord
        [HttpGet("{playerName}")]
        public void Leave(string playerName)
        {
            _server.LeaveGame(playerName);
        }
    }
}