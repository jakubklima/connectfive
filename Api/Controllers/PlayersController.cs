﻿using System;
using ConnectFive.ServerLogic;
using Microsoft.AspNetCore.Mvc;

namespace ConnectFive.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class PlayersController : Controller
    {
        private readonly IServer _server;

        public PlayersController(IServer server)
        {
            _server = server ?? throw new ArgumentNullException(nameof(server));
        }

        //api/players/trololord
        [HttpGet("{playerName}")]
        public Player Get(string playerName)
        {
            return _server.GetPlayer(playerName);
        }
    }
}